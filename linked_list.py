class Node:
    def __init__(self, data, n= None):
        self.data = data  # linked list part to hold the data
        self.next_node = n  # Initialized to null first time

    def get_data(self):
        return self.data

    def set_data(self, data):
        self.data = data

    def set_next(self, next_node):
        self.next_node = next_node

    def get_next(self):
        return self.next_node


class LinkedList(object):

    def __init__(self):
        print("linked list constructor")
        self.head = None
        self.size = 0

    def get_size(self):
        return self.size

    def add_node(self, d):
        new_node = Node(d, self.head)
        new_node.set_data(d)
        new_node.set_next(None)

        if new_node:
            new_node.next_node = self.head
            self.head = new_node
        else:
            self.head.set_next(new_node)
            new_node.set_next(None)
            new_node = new_node.get_next()

        self.size += 1

    def remove_node(self, d):
        this_node = self.head   #pointer to current location
        previous_node = None    #pointer to hold the previous node value

        while this_node:
            if this_node.get_data() == d:
                if previous_node:
                    previous_node.set_next(this_node.get_next())
                else:
                    self.head = this_node
                self.size -= 1
                return True
            else:
                previous_node = this_node
                this_node = this_node.get_next()
        return False

    def find(self, d):
        this_node = self.head
        pos = 0
        while this_node:
            pos += 1
            if this_node.get_data() == d:
                return d,pos
            else:
                this_node = this_node.get_next()
        return None

    def display_node(self):
        this_node = self.head
        while this_node:
            print(this_node.get_data())
            this_node = this_node.get_next()


def main():
    ll_obj = LinkedList()
    ll_obj.add_node(12)
    ll_obj.add_node(123)
    ll_obj.add_node(1231)
    ll_obj.add_node(1232)
    ll_obj.add_node(1233)
    ll_obj.remove_node(12)
    print(ll_obj.find(12))
    ll_obj.display_node()

if __name__ == '__main__':
    main()